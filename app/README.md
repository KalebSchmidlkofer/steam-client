# Shortcut Helper

This app speeds up the process of adding non-Steam shortcuts to Steam.

[Download the app](dist/Add Shortcut.exe) then drag and drop a file to begin.

It will follow Windows shortcuts and symlinks to find the real target, so you can drop a Shortcut onto the app as well.

![screenshot1](https://i.imgur.com/3DfsZ7A.png)

![screenshot2](https://i.imgur.com/T1moTsp.png)

## [Watch Demo on Steamable](https://streamable.com/e6fkb)

# Building the exe

```
pip install pypiwin32 eel pyinstaller
python -m eel "./Add Shortcut.py" web --noconsole --onefile --icon ./web/favicon.ico --name "Add Shortcut"
```

# To Do

* Be able to add multiple games at once