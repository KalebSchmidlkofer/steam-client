function load_info(){
    eel.get_file_info()(function(info){
        console.log(info);
        document.getElementById("name").value = info['name']
        document.getElementById("target").value = info['target']
        document.getElementById("start_dir").value = info['start_dir']
        document.getElementById("icon").value = info['icon']
        document.getElementById("launch_options").value = info['launch_options']
        document.getElementById("openvr").checked = info['openvr']
        document.getElementById("allow_overlay").checked = info['allow_overlay']
        document.getElementById("tags").checked = info['tags']
    });
}

function add_shortcut(){
    document.getElementById("submit").classList.add('is-loading');
    document.getElementById("submit").classList.add('disabled');
    eel.add_shortcut(document.getElementById("name").value,
                     document.getElementById("target").value,
                     document.getElementById("start_dir").value,
                     document.getElementById("icon").value,
                     document.getElementById("launch_options").value,
                     document.getElementById("openvr").checked,
                     document.getElementById("allow_overlay").checked,
                     document.getElementById("tags").value,
    )(function(ret){
        console.log("Ret:");
        console.log(ret);
        shortcut_result = ret[0];
        steam_running = ret[1];
        if (shortcut_result == 0){
            // It worked, disable the submit button
            var button = document.getElementById("submit");
            button.classList.add('is-success');
            button.classList.remove('is-loading');
            button.classList.remove('is-link');
            button.classList.remove('is-error');
            button.classList.remove('is-warning');
            button.onclick = null;
            button.innerHTML = "Success!";
            button.style.cursor = "default";
            document.getElementById("output").innerHTML = "(Restart Steam to see changes)"; 
            rst_btn = document.getElementById("restart");
            if (steam_running){
                rst_btn.innerHTML = "Restart Steam"
            } else {
                rst_btn.innerHTML = "Launch Steam"
            }
            rst_btn.style.display = "block";

            // Enable Close button as well
            document.getElementById("close").style.display = "block";

        } else {
            // Did not work, let the user know
            console.log("Something went wrong!");
            if (shortcut_result == 1){
                // Shortcut already exists
                document.getElementById("submit").classList.add('is-warning');
                document.getElementById("output").innerHTML = "Shortcut already exists! (Change the name)";
            } else if (shortcut_result == 2){
                // Missing name/exe
                document.getElementById("submit").classList.add('is-warning');
                document.getElementById("output").innerHTML = "Empty name or exe!";
            }
            document.getElementById("submit").classList.remove('is-link');
            document.getElementById("submit").classList.remove('is-loading');
        }
    });
}

function restart_steam(){
    document.getElementById("output").innerHTML = "(Restarting Steam)"; 
    button = document.getElementById("restart");
    button.classList.add('is-loading');
    button.classList.add('disabled');
    eel.restart_steam()(function(ret){
        console.log("Steam PID: " + ret);
        if (ret){
            console.log("Success!");
            button.classList.add("is-success");
            button.innerHTML = "Launched Steam";
            button.onclick = null;
            button.style.cursor = "default";
        } else {
            console.log("Error!");
            button.classList.add("is-warning");
        }
        button.classList.remove("is-link");
        button.classList.remove('is-loading');
    });
}

function stop(){
    window.close();
    eel.stop();
}