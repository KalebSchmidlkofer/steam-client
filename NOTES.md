## Running tests

```bash
python setup.py develop
pytest steamclient/tests
```

## Generating docs

```bash
pydocmd simple steamclient++ > DOCS.md
```

## Deploying to PyPi

```bash
python -m pip install wheel twine
python setup.py bdist_wheel
python -m twine upload --skip-existing dist/*.whl
```

# Old Notes

## Generating the filenames

The filename for the cover art is simply the app ID + 'p.png'.
The header is app ID + '_hero.png', and the logo is app ID + '_logo.png'.

This is easy for steam games, as we can look at the app_manifest files to get a list of games installed.
However, in order to get the IDs of non-steam shortcuts, we must read and parse the binary file Steam/userdata/\[steamid]/config/shortcuts.vdf. 
[Outdated Documentation](https://github.com/CorporalQuesadilla/Steam-Shortcut-Manager/wiki/Steam-Shortcuts-Documentation).

## Parsing shortcuts.vdf

The contents of shortcuts.vdf are read in binary format and split up by application.

```python
path = f"{userdata_path}\\config\\shortcuts.vdf"
    with open(path, 'rb') as f:
        data = f.read()
    return data.split(b'\x01AppName\x00')[1:]
```

By splitting on the 'AppName' delimiter, we can look at each individual entry:

```python
[b'Minecraft', 
b'\x01Exe', b'"C:\\Program Files (x86)\\Minecraft Launcher\\MinecraftLauncher.exe"', 
b'\x01StartDir', b'"C:\\Program Files (x86)\\Minecraft Launcher\\"', 
b'\x01icon', b'', 
b'\x01ShortcutPath', b'', 
b'\x01LaunchOptions', b'', 
b'\x02IsHidden', b'', b'', b'', b'', 
b'\x02AllowDesktopConfig', b'\x01', b'', b'', 
b'\x02AllowOverlay', b'\x01', b'', b'', 
b'\x02openvr', b'', b'', b'', b'', 
b'\x02Devkit', b'', b'', b'', b'', 
b'\x01DevkitGameID', b'', 
b'\x02LastPlayTime', b'', b'', b'', b'', b'', 
b'tags', b'\x010', b'favorite', b'\x011', b'Grid beauty', 
b'\x08\x08']
```

These values are then parsed into Shortcut objects with the fields decoded.
The app ID of a non-steam shortcut is a crc32 hash of the target concatenated with the name of the game.

```python
class Shortcut():
    def __init__(self, entry):
        self._entry = entry.split(b'\x00')
        self.name = self._entry[0].decode()
        self.exe = self._entry[2].decode()
        self._id = None

    @property
    def id(self):
        if self._id is None:
            s = self.exe + self.name
            self._id = str((zlib.crc32(s.encode()) & 0xffffffff) | 0x80000000)
        return self._id
```
